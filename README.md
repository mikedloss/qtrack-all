# qtrack (prototype)
Qtrack is a simple tool that lets you search for technology requests (from GRACIE). In return, you get insights into what progress has been made in TFS by our technology team.

## Install
To run the node server
1. navigate to the root folder
2. Make a copy of `config.example.env` as `config.env`
3. Fill out the `config.env` file
2. run `yarn` (or `npm install`) to install dependencies
3. run `yarn run start` (or `npm run start`) to run the server

To run the Webpack server for the front-end development
1. open up a 2nd terminal
2. navigate to the /client/ folder
3. run `yarn run start` (or `npm run start`) to run the live dev server

You can also run `yarn run build` from inside the client folder to build the app, which the node server will point at once it exists if you *aren't* already running the UI from the Webpack server.

## Questions
> Q: Why?

A: I don't like logging into GRACIE. It can be slow to search (sometimes not even working at all). Technology team members don't update tech requests with the current status of the request, only notes before/during prioritization and an update when it's done. This little tools sets out to solve that. Business team members can search for a GRACIE id, and they'll get info about where the request is at in TFS.

Technology team members should be communicating during the build-out of a request *regardless*, but this can be used just for an additional way to help gain clarity into what Technology does with the request once it gets thrown over the wall that is "Gracie to TFS".

> Q: They shouldn't see all that info!!!!!!

A: I agree. That's why I'm only giving them "insights" into it. They don't need to see every piece of information, but they should have a clearer picture of what's going on. GRACIE doesn't allow for that (today), nor is it an easy thing for technology to begin using immediately, so here's an alternative.

> Q: Why is this a "prototype"?

A: There might be a .NET WebAPI implementation coming to handle some limitations to GRACIE. Most other applications that utilize GRACIE information use data feeds and consume some XML export from a report. That can be done at timed intervals throughout the day, which means that the data won't be "live". While that is beneficial because technology doesn't go in and update GRACIE anyways, it does require a process running on an interval to consume the report, then save everything to a database, and then to query all of that. The WebAPI implementation is looking at making all of that possible.

Another thing, the GRACIE API is bad. Imagine doing 3 separate API calls to determine what is chosen in a dropdown. I'm sure there can be a better way to doing it (by all means contribute), but that's what needs to be done for every single values list field in a GRACIE record.

Lastly, since it's using 1 account to login, query, and logout, there might be issues with multiple sessions. I can't test that until it's live and multiple people use it, but that's a definitely a possible limitation. Thankfully, the GRACIE calls all *should* finish, at most, within 2 seconds, so the likelihood is *slim* but possible. Implementing a queue won't work because of the authorization header requirement for every single API call (except the login). It might be possible to use a queue with multiple GRACIE logins, but that seems dirty. Or, we could use LDAP GRACIE accounts, but that hasn't been tested yet.

Again, if **you** can come up with a better solution, by all means contribute!

> Q: Why didn't you use PHP/C#/MVC/Angular2+/jquery/assembly/some other language?

A: Bullet time.

### Contributing
1) Fork the repository
2) Create a branch with some descriptive name
3) Pull request against `mdloss/master`
4) I'll review/merge if appropriate

I'm open to features / refactoring, just make a good case for it 😁

### Technology Used
##### Front end
- React
- Bootstrap
- Axios
##### Back-end
- Node
- Express
- Axios
##### Services
- GRACIE REST API
- TFS API
#### Other stuff
- Node 8
- Yarn