import styled from 'styled-components';

const AccordionEmpty = styled.div`
  display: block;
  padding: .75rem 1.25rem;
  margin-bottom: 0;
  background-color: rgba(0,0,0,.03);
  border: 1px solid rgba(0,0,0,.125);
  box-sizing: border-box;
  border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
`

export default AccordionEmpty;