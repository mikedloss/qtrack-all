import React from 'react';
import styled from 'styled-components';

const ExpandableInfoContainer = styled.div`
  padding-top: 1em;
`

const ExpandableInfo = (props) => {
  let htmlstrip = "<(?:[^>=]|='[^']*'|=\"[^\"]*\"|=[^'\"][^>]*)*>";  
  let title = props.title;
  let titleId = title.replace(' ', '');
  let data = props.data.replace(htmlstrip, '');
  return (
    <ExpandableInfoContainer role='tablist'>
      <div className='card'>
        <div className='card-header' role='tab' id={`${title}Header`}>
          <h5 style={{'display': 'inline-block'}}>
            <a className='collapsed' data-toggle='collapse' href={`#${titleId}CollapseGracie`} aria-expanded='false'>
              {title}
            </a>  
          </h5>
          <span className='text-muted' style={{'paddingLeft': '10px'}}>click to expand</span>
        </div>
        <div id={`${titleId}CollapseGracie`} className='collapse' role='tabpanel'>
          <div className='col-md-12'>
            <p className="card-text" dangerouslySetInnerHTML={{__html: data}}></p>
          </div>
        </div>
      </div>
    </ExpandableInfoContainer>
  )
}

export default ExpandableInfo;