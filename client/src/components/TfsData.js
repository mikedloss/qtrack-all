import React from 'react';
import ExpandableInfo from '../components/ExpandableInfo';
import AccordionEmpty from './AccordionEmpty';
import InfoField from './InfoField';

const TfsData = (tfs) => {
  let data = tfs.data;
  let htmlstrip = "<(?:[^>=]|='[^']*'|=\"[^\"]*\"|=[^'\"][^>]*)*>";

  // console.log(data);
  return (
    <div className="card-body">
      <h4 className="card-title">{data.fields['System.Title']}</h4>
      <h6 className="card-subtitle mb-2">
        <span className="badge badge-primary">{data.fields['System.State']}</span>
      </h6>
      {(data.fields.hasOwnProperty('System.Description')) 
        ? (<ExpandableInfo title='Description' data={data.fields['System.Description'].replace(htmlstrip, "")} />)
        : (<AccordionEmpty>No Description found</AccordionEmpty>)
      }
      <br/>
      <div className='row'>
        {(data.fields.hasOwnProperty('Microsoft.VSTS.Scheduling.TargetDate'))
          ? (<InfoField title='Target Date' data={data.fields['Microsoft.VSTS.Scheduling.TargetDate']} classes='col-md-6 col-12' />)
          : (<InfoField title='Target Date' data='Not Found' classes='col-md-6 col-12' />)
        }
        {(data.fields.hasOwnProperty('QL.Team'))
          ? (<InfoField title='Team' data={data.fields['QL.Team']} classes='col-md-6 col-12' />)
          : (<InfoField title='Team' data='Not Found' classes='col-md-6 col-12' />)
        }
      </div>
    </div>
  );
}

export default TfsData;