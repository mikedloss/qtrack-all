import React, { Component } from 'react';
import styled from 'styled-components';
import GracieData from '../components/GracieData';

const GracieInfo = styled.div`
  padding-top: 5%;
`

const GracieCard = styled.div`
  position: relative;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-direction: column;
  flex-direction: column;
  min-width: 0;
  word-wrap: break-word;
  background-color: #fff;
  background-clip: border-box;
  border: 1px solid rgba(0,0,0,.125);
  border-radius: .25rem;
  border-left: 6px solid #064270;
`

class Gracie extends Component {
  componentWillUnmount = (data) => {
    data = '';
  }

  render() {
    let data = this.props.data;
    return (
      <GracieInfo>
        <h5>GRACIE</h5>
        <GracieCard>
        {(!data.getItemSuccess) &&
          <div className="card-body">
            <h4 className="card-title">Error</h4>
            <p className="card-text">{data.error}</p>
          </div>
        }
        {(data.getItemSuccess) &&
          <GracieData data={this.props.data.data} />
        }
        </GracieCard>
      </GracieInfo>
    );
  }
}

export default Gracie;