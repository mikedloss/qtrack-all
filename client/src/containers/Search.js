import React, { Component } from 'react';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonGracie: 'btn-outline-primary',
      buttonTfs: 'btn-outline-primary'      
    };

    this.handleInput = this.handleInput.bind(this);
    this.handleChangeSearch = this.handleChangeSearch.bind(this);
  }

  handleInput = (e) => {
    this.setState({ userInput: e.target.value });
  }

  handleChangeSearch = (selected) => {
    this.props.selected(selected);
    if (selected === 'GRACIE') {
      this.setState({ 
        buttonGracie: 'btn-primary',
        buttonTfs: 'btn-outline-primary'
       });
    } else {
      this.setState({ 
        buttonTfs: 'btn-primary',
        buttonGracie: 'btn-outline-primary'
      });
    }
  }

  componentDidMount = () => {
    // only used until the TFS portion is built
    this.handleChangeSearch('GRACIE');
  }

  render() {
    return (
      <div className='search'>
        <div className='row'>
          <div className='col-md-10 col-12'>
            <div className='input-group'>
              <input type="text" className="form-control" placeholder="Search for..." aria-label="Search for..." onChange={this.props.input} />
            </div>
          </div>
          <br/>
          <div className='col-md-2 col-12'>
            <div className="btn-group d-flex" role="group">
              <button id='gracie-button' type="button" className={`btn ${this.state.buttonGracie} w-100`} onClick={() => this.handleChangeSearch('GRACIE')}>GRACIE</button>
              {/*<button id='tfs-button' type="button" className={`btn ${this.state.buttonTfs} w-100`} onClick={() => this.handleChangeSearch('TFS')}>TFS</button>*/}
            </div>        
          </div>
        </div>
        <br/>
        <div className='row'>
          <div className='col-md-12'>
            <button type="button" className="btn btn-outline-success btn-block" onClick={this.props.search}>Go</button>       
          </div>
        </div>
      </div>
    );
  }
}

export default Search;