import React, { Component } from 'react';
import toastr from 'toastr';
import { Switch, Route } from 'react-router-dom'
import styled from 'styled-components';

import Header from './Header';
import Main from './Main';
import Help from '../components/Help';
import Footer from './Footer';

const AppContainer = styled.div`
  margin-top: 2%;
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
  @media (min-width: 576px) {
    max-width: 540px;
  }
  
  @media (min-width: 768px) {
    max-width: 720px;
  }
  
  @media (min-width: 992px) {
    max-width: 960px;
  }
  
  @media (min-width: 1200px) {
    max-width: 1140px;
  }
`

class App extends Component {
  componentDidMount = () => {
    toastr.options.preventDuplicates = true;
  }
  
  render() {
    return (
      <AppContainer>
          <Header />
          <Switch>
            <Route exact path='/help' component={Help} />
            <Route exact path='/' component={Main} />
          </Switch>
          <Footer />
      </AppContainer>
    );
  }
}

export default App;
