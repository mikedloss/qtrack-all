import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom'

const TitleHeader = styled.p`
  font-size: 40px;
  font-weight: bold;
  line-height: 40px;
`

const HelperLinks = styled.ul`
  list-style: none;
  padding-left: 0;
  padding-bottom: 2%;
`

const HelperLink = styled.li`
  display: inline;
  padding-right: 1em;
`

const StyledLink = styled(Link)`
  color: black;
  text-decoration: none;
`

const StyledHeaderLink = styled(StyledLink)`
  &:hover {
    color: black;
    text-decoration: none;
  }
`

const Header = () => {
  return (
    <div className='header'>
      <div className='row'>
        <div className='col-12'>
          <TitleHeader>
            <StyledHeaderLink to='/'>QTrack</StyledHeaderLink>
          </TitleHeader>
        </div>
      </div>
      <HelperLinks>
        <HelperLink>
          <StyledLink to='/help'>Help</StyledLink>
        </HelperLink>
      </HelperLinks>
    </div>
  )
}

export default Header;