'use strict';

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

// const express = require('express');
var https = require('https');
var camelCase = require('camelcase');
var axios = require('axios');
var sanitizeHtml = require('sanitize-html');
var striptags = require('striptags');

// const router = express.Router();
var graciefields = require('../../../static/fielddecode.json');

exports.getItem = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var errors, allErrors, error, loginReply, getItemReply, prettyItem, logoutReply;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            // check if we're using home dev info
            req.checkParams('id', 'ID is not a number').isInt();
            errors = req.validationErrors();

            if (!errors) {
              _context.next = 8;
              break;
            }

            allErrors = '';

            for (error in errors) {
              allErrors += error.msg + ' ';
            }
            res.status('400');
            res.json({
              error: { errors },
              errorString: allErrors
            });
            return _context.abrupt('return');

          case 8:
            _context.next = 10;
            return gracieLogin();

          case 10:
            loginReply = _context.sent;

            if (loginReply.loginSuccess) {
              _context.next = 16;
              break;
            }

            res.status('401').send(loginReply);
            return _context.abrupt('return');

          case 16:
            _context.next = 18;
            return getGracieItem(loginReply.authId, req.params.id);

          case 18:
            getItemReply = _context.sent;

            if (getItemReply.getItemSuccess) {
              _context.next = 22;
              break;
            }

            res.json(getItemReply);
            return _context.abrupt('return');

          case 22:
            _context.next = 24;
            return prettifyGracieItem(getItemReply.item, loginReply.authId);

          case 24:
            prettyItem = _context.sent;
            _context.next = 27;
            return gracieLogout(loginReply.authId);

          case 27:
            logoutReply = _context.sent;

            // console.log(`Logged out`);

            if (loginReply.loginSuccess && getItemReply.getItemSuccess && logoutReply.logoutSuccess) {
              res.json(prettyItem);
            }

          case 29:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

var gracieLogin = function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
    var url, reply, data;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            // returns Authorization Session ID
            url = getEnvironmentURL() + `security/login`;
            reply = {};
            data = {};
            _context2.next = 5;
            return axios(getAxiosGracieOptions(url, 'POST')).then(function (response) {
              data = response.data;
              if (response.data.IsSuccessful) {
                reply = {
                  loginSuccess: true,
                  authId: response.data.RequestedObject.SessionToken
                };
                // console.log(`Logged in with ID: ${response.data.RequestedObject.SessionToken}`);
              } else {
                reply = {
                  loginSuccess: false,
                  error: response.data
                };
                console.log(`Error logging in: ${response.data.ValidationMessages[0].MessageKey}`);
              }
            }).catch(function (response) {
              return reply = response;
            });

          case 5:
            _context2.next = 7;
            return reply;

          case 7:
            return _context2.abrupt('return', _context2.sent);

          case 8:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, undefined);
  }));

  return function gracieLogin() {
    return _ref2.apply(this, arguments);
  };
}();

var getGracieItem = function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(authId, itemID) {
    var url, reply, data;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            url = getEnvironmentURL() + `content/${itemID}`;
            // console.log(url);

            reply = {};
            data = {};
            _context3.next = 5;
            return axios(getAxiosGracieOptions(url, 'GET', authId)).then(function (response) {
              data = response.data;
              if (response.data.IsSuccessful) {
                reply = {
                  getItemSuccess: true,
                  item: response.data.RequestedObject
                };
              } else {
                reply = {
                  getItemSuccess: false,
                  error: response.data.ValidationMessages[0].Description
                };
              }
            });

          case 5:
            _context3.next = 7;
            return reply;

          case 7:
            return _context3.abrupt('return', _context3.sent);

          case 8:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, undefined);
  }));

  return function getGracieItem(_x3, _x4) {
    return _ref3.apply(this, arguments);
  };
}();

var gracieLogout = function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(authId) {
    var url, reply, data;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            url = getEnvironmentURL() + `security/logout`;
            reply = {};
            data = {};
            _context4.next = 5;
            return axios(getAxiosGracieOptions(url, 'POST', authId)).then(function (response) {
              data = response.data;
              if (response.data.IsSuccessful) {
                reply = {
                  logoutSuccess: true,
                  message: 'Successfully logged out'
                };
                // console.log(`Successfully logged out`);
              } else {
                reply = {
                  logoutSuccess: false,
                  message: 'Error logging out'
                };
                console.log(`Error logging out`);
              }
            });

          case 5:
            _context4.next = 7;
            return reply;

          case 7:
            return _context4.abrupt('return', _context4.sent);

          case 8:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, undefined);
  }));

  return function gracieLogout(_x5) {
    return _ref4.apply(this, arguments);
  };
}();

var getFieldDefinitions = function () {
  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(levelId, authId) {
    var url, data, relatedValuesListId;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            url = getEnvironmentURL() + `system/fielddefinition/level/${levelId}`;
            // console.log(url);

            data = {};
            relatedValuesListId = '';
            // console.log(authId);
            // console.log(getAxiosGracieOptions(url, 'GET', authId));

            _context5.next = 5;
            return axios(getAxiosGracieOptions(url, 'GET', authId)).then(function (response) {
              data = response.data;
            });

          case 5:
            _context5.next = 7;
            return data;

          case 7:
            return _context5.abrupt('return', _context5.sent);

          case 8:
          case 'end':
            return _context5.stop();
        }
      }
    }, _callee5, undefined);
  }));

  return function getFieldDefinitions(_x6, _x7) {
    return _ref5.apply(this, arguments);
  };
}();

var getValuesListValue = function () {
  var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(fieldDefs, fieldId, valueId, authId) {
    var realValueId, url, reply;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            // search field definitions for the real values list ID
            realValueId = '';

            fieldDefs.map(function (field) {
              if (field.RequestedObject.Id == fieldId) {
                realValueId = field.RequestedObject.RelatedValuesListId;
              }
            });

            // use that ID to query a different endpoint (i <3 gracie)
            url = getEnvironmentURL() + `system/valueslistvalue/flat/valueslist/${realValueId}`;
            reply = '';
            _context6.next = 6;
            return axios(getAxiosGracieOptions(url, 'GET', authId)).then(function (response) {
              var _iteratorNormalCompletion = true;
              var _didIteratorError = false;
              var _iteratorError = undefined;

              try {
                for (var _iterator = response.data[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                  var value = _step.value;

                  if (value.RequestedObject.Id == valueId) {
                    // console.log('Found ValueID! : ' + valueId + ' - ' + value.RequestedObject.Name);
                    reply = value.RequestedObject.Name;
                  }
                }
              } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
              } finally {
                try {
                  if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                  }
                } finally {
                  if (_didIteratorError) {
                    throw _iteratorError;
                  }
                }
              }
            });

          case 6:
            _context6.next = 8;
            return reply;

          case 8:
            return _context6.abrupt('return', _context6.sent);

          case 9:
          case 'end':
            return _context6.stop();
        }
      }
    }, _callee6, undefined);
  }));

  return function getValuesListValue(_x8, _x9, _x10, _x11) {
    return _ref6.apply(this, arguments);
  };
}();

var getUserListValue = function () {
  var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(userId, authId) {
    var url, data, reply;
    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            // console.log(userId);
            // console.log(authId);
            url = getEnvironmentURL() + `system/user/${userId}`;
            data = {};
            reply = '';
            // console.log(url);

            _context7.next = 5;
            return axios(getAxiosGracieOptions(url, 'GET', authId)).then(function (response) {
              reply = response.data.RequestedObject.DisplayName;
            });

          case 5:
            _context7.next = 7;
            return reply;

          case 7:
            return _context7.abrupt('return', _context7.sent);

          case 8:
          case 'end':
            return _context7.stop();
        }
      }
    }, _callee7, undefined);
  }));

  return function getUserListValue(_x12, _x13) {
    return _ref7.apply(this, arguments);
  };
}();

var prettifyGracieItem = function () {
  var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(item, authId) {
    var prettyItem, data, fieldIDs, recordId, createDate, updateDate, levelId, fieldDefinitions, entries, fields, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, field, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, _field, type, _iteratorNormalCompletion4, _didIteratorError4, _iteratorError4, _iterator4, _step4, each, _iteratorNormalCompletion5, _didIteratorError5, _iteratorError5, _iterator5, _step5, _each;

    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            // need field IDs (get from JSON object by titles)
            prettyItem = {};
            data = {};
            fieldIDs = {};
            recordId = item.Id;
            createDate = item.UpdateInformation.CreateDate;
            updateDate = item.LastUpdated;
            levelId = item.LevelId;
            // console.log('getting field definitions');

            _context8.next = 9;
            return getFieldDefinitions(levelId, authId);

          case 9:
            fieldDefinitions = _context8.sent;

            // console.log(fieldDefinitions);
            entries = Object.entries(item.FieldContents);
            // console.log(entries[1]);
            // console.log(fieldDefinitions[0]);

            fields = fieldDefinitions.filter(function (field) {
              return graciefields.includes(field.RequestedObject.Name);
            });
            // console.log(fields[2]);

            _iteratorNormalCompletion2 = true;
            _didIteratorError2 = false;
            _iteratorError2 = undefined;
            _context8.prev = 15;
            for (_iterator2 = fields[Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              field = _step2.value;

              fieldIDs[field.RequestedObject.Id] = field.RequestedObject.Name;
            }
            // console.log('-----');
            // console.log(fieldIDs);
            // console.log(graciefields);
            // console.log(entries);
            _context8.next = 23;
            break;

          case 19:
            _context8.prev = 19;
            _context8.t0 = _context8['catch'](15);
            _didIteratorError2 = true;
            _iteratorError2 = _context8.t0;

          case 23:
            _context8.prev = 23;
            _context8.prev = 24;

            if (!_iteratorNormalCompletion2 && _iterator2.return) {
              _iterator2.return();
            }

          case 26:
            _context8.prev = 26;

            if (!_didIteratorError2) {
              _context8.next = 29;
              break;
            }

            throw _iteratorError2;

          case 29:
            return _context8.finish(26);

          case 30:
            return _context8.finish(23);

          case 31:
            _iteratorNormalCompletion3 = true;
            _didIteratorError3 = false;
            _iteratorError3 = undefined;
            _context8.prev = 34;
            _iterator3 = entries[Symbol.iterator]();

          case 36:
            if (_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done) {
              _context8.next = 112;
              break;
            }

            _field = _step3.value;

            if (!fieldIDs.hasOwnProperty(_field[0])) {
              _context8.next = 109;
              break;
            }

            // console.log('FOUND! ' + field[0] + ' - ' + camelCase(fieldIDs[field[0]]) + ' : ' + field[1].Type);
            // types:
            // 1 = string (includes HTML, will strip)
            // 2 = number? tfsRequestId is a field
            // 3 = date
            // 4 = values list
            // 6 = tracking ID (primary key) (do not use)
            // 8 = names (users)
            // 9 = related records
            type = _field[1].Type;

            if (!(type == 1 || type == 2 || type == 3)) {
              _context8.next = 45;
              break;
            }

            // console.log('Found! Type 1/2/3: ' + field[0]);
            // console.log(field[1].Value);
            data[camelCase(fieldIDs[_field[0]])] = '';
            if (_field[1].Value != null) {
              data[camelCase(fieldIDs[_field[0]])] = striptags(sanitizeHtml(_field[1].Value));
            }
            _context8.next = 109;
            break;

          case 45:
            if (!(type == 4)) {
              _context8.next = 77;
              break;
            }

            // console.log('Found! Type 4: ' + field[0]);
            // console.log(field[1].Value);
            data[camelCase(fieldIDs[_field[0]])] = [];

            if (!(_field[1].Value != null)) {
              _context8.next = 75;
              break;
            }

            _iteratorNormalCompletion4 = true;
            _didIteratorError4 = false;
            _iteratorError4 = undefined;
            _context8.prev = 51;
            _iterator4 = _field[1].Value.ValuesListIds[Symbol.iterator]();

          case 53:
            if (_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done) {
              _context8.next = 61;
              break;
            }

            each = _step4.value;
            _context8.next = 57;
            return getValuesListValue(fieldDefinitions, _field[0], each, authId);

          case 57:
            data[camelCase(fieldIDs[_field[0]])] = _context8.sent;

          case 58:
            _iteratorNormalCompletion4 = true;
            _context8.next = 53;
            break;

          case 61:
            _context8.next = 67;
            break;

          case 63:
            _context8.prev = 63;
            _context8.t1 = _context8['catch'](51);
            _didIteratorError4 = true;
            _iteratorError4 = _context8.t1;

          case 67:
            _context8.prev = 67;
            _context8.prev = 68;

            if (!_iteratorNormalCompletion4 && _iterator4.return) {
              _iterator4.return();
            }

          case 70:
            _context8.prev = 70;

            if (!_didIteratorError4) {
              _context8.next = 73;
              break;
            }

            throw _iteratorError4;

          case 73:
            return _context8.finish(70);

          case 74:
            return _context8.finish(67);

          case 75:
            _context8.next = 109;
            break;

          case 77:
            if (!(type == 8)) {
              _context8.next = 109;
              break;
            }

            // console.log('Found! Type 8: ' + field[0]);
            // console.log(field[1].Value);
            data[camelCase(fieldIDs[_field[0]])] = [];

            if (!(_field[1].Value != null)) {
              _context8.next = 109;
              break;
            }

            _iteratorNormalCompletion5 = true;
            _didIteratorError5 = false;
            _iteratorError5 = undefined;
            _context8.prev = 83;
            _iterator5 = _field[1].Value.UserList[Symbol.iterator]();

          case 85:
            if (_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done) {
              _context8.next = 95;
              break;
            }

            _each = _step5.value;
            _context8.t2 = data[camelCase(fieldIDs[_field[0]])];
            _context8.next = 90;
            return getUserListValue(_each.Id, authId);

          case 90:
            _context8.t3 = _context8.sent;

            _context8.t2.push.call(_context8.t2, _context8.t3);

          case 92:
            _iteratorNormalCompletion5 = true;
            _context8.next = 85;
            break;

          case 95:
            _context8.next = 101;
            break;

          case 97:
            _context8.prev = 97;
            _context8.t4 = _context8['catch'](83);
            _didIteratorError5 = true;
            _iteratorError5 = _context8.t4;

          case 101:
            _context8.prev = 101;
            _context8.prev = 102;

            if (!_iteratorNormalCompletion5 && _iterator5.return) {
              _iterator5.return();
            }

          case 104:
            _context8.prev = 104;

            if (!_didIteratorError5) {
              _context8.next = 107;
              break;
            }

            throw _iteratorError5;

          case 107:
            return _context8.finish(104);

          case 108:
            return _context8.finish(101);

          case 109:
            _iteratorNormalCompletion3 = true;
            _context8.next = 36;
            break;

          case 112:
            _context8.next = 118;
            break;

          case 114:
            _context8.prev = 114;
            _context8.t5 = _context8['catch'](34);
            _didIteratorError3 = true;
            _iteratorError3 = _context8.t5;

          case 118:
            _context8.prev = 118;
            _context8.prev = 119;

            if (!_iteratorNormalCompletion3 && _iterator3.return) {
              _iterator3.return();
            }

          case 121:
            _context8.prev = 121;

            if (!_didIteratorError3) {
              _context8.next = 124;
              break;
            }

            throw _iteratorError3;

          case 124:
            return _context8.finish(121);

          case 125:
            return _context8.finish(118);

          case 126:

            // console.log(data);
            // console.log(item);
            // console.log(recordId);
            prettyItem = {
              getItemSuccess: true,
              recordId: recordId,
              createDate: createDate,
              updateDate: updateDate,
              data: data,
              loginSuccess: true
            };

            return _context8.abrupt('return', prettyItem);

          case 128:
          case 'end':
            return _context8.stop();
        }
      }
    }, _callee8, undefined, [[15, 19, 23, 31], [24,, 26, 30], [34, 114, 118, 126], [51, 63, 67, 75], [68,, 70, 74], [83, 97, 101, 109], [102,, 104, 108], [119,, 121, 125]]);
  }));

  return function prettifyGracieItem(_x14, _x15) {
    return _ref8.apply(this, arguments);
  };
}();

var getEnvironmentURL = function getEnvironmentURL() {
  if (process.env.GRACIEENVIRONMENT.toUpperCase() == 'PRODUCTION') {
    return `https://gracie/api/core/`;
  } else {
    return `https://gracie${process.env.GRACIEENVIRONMENT}/api/core/`;
  }
};

var getAxiosGracieOptions = function getAxiosGracieOptions(url, method) {
  var authId = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

  var options = {
    method: `${method.toUpperCase()}`,
    url: `${url}`,
    headers: {
      'Accept': 'application/json, text/html, application/xhtml+xml, application/xml',
      'Content-Type': 'application/json',
      'Authorization': `Archer session-id="${authId}"`
    },
    httpsAgent: new https.Agent({
      rejectUnauthorized: false
    })
  };

  if (url.includes('login')) {
    var environment = process.env.GRACIEENVIRONMENT;
    var username = process.env.GRACIEUSERNAME;
    var password = process.env.GRACIEPASSWORD;
    options.data = {
      'InstanceName': `GRACIE${environment.toUpperCase()}`,
      'UserDomain': '',
      'Username': `${username}`,
      'Password': `${password}`
    };
  }

  if (url.includes('logout')) {
    options.data = {
      'Value': `${authId}`
    };
  }

  return options;
};