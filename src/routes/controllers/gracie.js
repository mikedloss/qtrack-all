// const express = require('express');
const https = require('https');
const camelCase = require('camelcase');
const axios = require('axios');
const sanitizeHtml = require('sanitize-html');
const striptags = require('striptags');

// const router = express.Router();
const graciefields = require('../../../static/fielddecode.json');

exports.getItem = async (req, res) => {
  // check if we're using home dev info
  req.checkParams('id', 'ID is not a number').isInt();
  const errors = req.validationErrors();
  if (errors) {
    let allErrors = '';
    for (let error in errors) {
      allErrors += error.msg + ' ';
    }
    res.status('400');
    res.json({
      error: {errors},
      errorString: allErrors
    });
    return;
  }

  // login
  // get record info based on ID
  // make that record info pretty
  // logout
  // send the prettied json object as the response
  let loginReply = await gracieLogin();
  if (!loginReply.loginSuccess) {
    res.status('401').send(loginReply);
    return;
  } else {
    // console.log(`Logged in`);
  
    // console.log(`Getting item info...`);
    let getItemReply = await getGracieItem(loginReply.authId, req.params.id);
    // console.log(getItemReply);
    if (!getItemReply.getItemSuccess) {
      res.json(getItemReply);
      return;
    }

    // turn this ugly mess into a pretty GRACIE record object
    // WARNING: GRACIE API sucks so there's a bunch of requests in here
    // res.json(getItemReply.item);
    // console.log('prettying the data');
    let prettyItem = await prettifyGracieItem(getItemReply.item, loginReply.authId);
    // console.log(prettyItem);
    // console.log('logging out');
    let logoutReply = await gracieLogout(loginReply.authId);
    // console.log(`Logged out`);
    
    if (loginReply.loginSuccess && getItemReply.getItemSuccess && logoutReply.logoutSuccess) {
      res.json(prettyItem);
    }
  }
}

const gracieLogin = async () => {
  // returns Authorization Session ID
  let url = getEnvironmentURL() + `security/login`;
  let reply = {};
  let data = {};
  await axios(
    getAxiosGracieOptions(url, 'POST')
  )
  .then((response) => {
    data = response.data;
    if (response.data.IsSuccessful) {
      reply = {
        loginSuccess: true,
        authId: response.data.RequestedObject.SessionToken
      };
      // console.log(`Logged in with ID: ${response.data.RequestedObject.SessionToken}`);
    } else {
      reply = {
        loginSuccess: false,
        error: response.data
      };
      console.log(`Error logging in: ${response.data.ValidationMessages[0].MessageKey}`);
    }
  })
  .catch((response) => reply = response);
  return await reply;
}

const getGracieItem = async (authId, itemID) => {
  let url = getEnvironmentURL() + `content/${itemID}`;
  // console.log(url);
  let reply = {};
  let data = {};
  await axios(
    getAxiosGracieOptions(url, 'GET', authId)
  )
  .then((response) => {
    data = response.data;
    if (response.data.IsSuccessful) {
      reply = {
        getItemSuccess: true,
        item: response.data.RequestedObject
      };
    } else {
      reply = {
        getItemSuccess: false,
        error: response.data.ValidationMessages[0].Description
      };
    }
  });
  return await reply;
}

const gracieLogout = async (authId) => {
  let url = getEnvironmentURL() + `security/logout`;
  let reply = {};
  let data = {};
  await axios(
    getAxiosGracieOptions(url, 'POST', authId)
  )
  .then((response) => {
    data = response.data;
    if (response.data.IsSuccessful) {
      reply = {
        logoutSuccess: true,
        message: 'Successfully logged out'
      };
      // console.log(`Successfully logged out`);
    } else {
      reply = {
        logoutSuccess: false,
        message: 'Error logging out'
      };
      console.log(`Error logging out`);
    }
  });
  return await reply;
}

const getFieldDefinitions = async (levelId, authId) => {
  let url = getEnvironmentURL() + `system/fielddefinition/level/${levelId}`;
  // console.log(url);
  let data = {};
  let relatedValuesListId = '';
  // console.log(authId);
  // console.log(getAxiosGracieOptions(url, 'GET', authId));
  await axios(
    getAxiosGracieOptions(url, 'GET', authId)
  )
  .then((response) => {
    data = response.data;
  });
  return await data;
}

const getValuesListValue = async (fieldDefs, fieldId, valueId, authId) => {
  // search field definitions for the real values list ID
  let realValueId = '';
  fieldDefs.map((field) => {
    if (field.RequestedObject.Id == fieldId) {
      realValueId = field.RequestedObject.RelatedValuesListId;
    }
  });

  // use that ID to query a different endpoint (i <3 gracie)
  let url = getEnvironmentURL() + `system/valueslistvalue/flat/valueslist/${realValueId}`;
  let reply = '';
  await axios(
    getAxiosGracieOptions(url, 'GET', authId)
  )
  .then((response) => {
    for (let value of response.data) {
      if (value.RequestedObject.Id == valueId) {
        // console.log('Found ValueID! : ' + valueId + ' - ' + value.RequestedObject.Name);
        reply = value.RequestedObject.Name;
      }
    }
  });
  
  return await reply;
}

const getUserListValue = async (userId, authId) => {
  // console.log(userId);
  // console.log(authId);
  let url = getEnvironmentURL() + `system/user/${userId}`;
  let data = {};
  let reply = '';
  // console.log(url);
  await axios(
    getAxiosGracieOptions(url, 'GET', authId)
  )
  .then((response) => {
    reply = response.data.RequestedObject.DisplayName;
  });

  return await reply;
}

const prettifyGracieItem = async (item, authId) => {
  // need field IDs (get from JSON object by titles)
  let prettyItem = {};
  let data = {};
  let fieldIDs = {};
  const recordId = item.Id;
  const createDate = item.UpdateInformation.CreateDate;
  const updateDate = item.LastUpdated;
  const levelId = item.LevelId;
  // console.log('getting field definitions');
  let fieldDefinitions = await getFieldDefinitions(levelId, authId);
  // console.log(fieldDefinitions);
  let entries = Object.entries(item.FieldContents);
  // console.log(entries[1]);
  // console.log(fieldDefinitions[0]);
  let fields = fieldDefinitions.filter((field) => {
    return graciefields.includes(field.RequestedObject.Name);
  });
  // console.log(fields[2]);
  for (let field of fields) {
    fieldIDs[field.RequestedObject.Id] = field.RequestedObject.Name;
  }
  // console.log('-----');
  // console.log(fieldIDs);
  // console.log(graciefields);
  // console.log(entries);
  for (let field of entries) {
    if (fieldIDs.hasOwnProperty(field[0])) {
      // console.log('FOUND! ' + field[0] + ' - ' + camelCase(fieldIDs[field[0]]) + ' : ' + field[1].Type);
      // types:
      // 1 = string (includes HTML, will strip)
      // 2 = number? tfsRequestId is a field
      // 3 = date
      // 4 = values list
      // 6 = tracking ID (primary key) (do not use)
      // 8 = names (users)
      // 9 = related records
      let type = field[1].Type;
      if (type == 1 || type == 2 || type == 3) {
        // console.log('Found! Type 1/2/3: ' + field[0]);
        // console.log(field[1].Value);
        data[camelCase(fieldIDs[field[0]])] = '';
        if (field[1].Value != null) {
          data[camelCase(fieldIDs[field[0]])] = striptags(sanitizeHtml(field[1].Value));
        }
      } else if (type == 4) {
        // console.log('Found! Type 4: ' + field[0]);
        // console.log(field[1].Value);
        data[camelCase(fieldIDs[field[0]])] = [];
        if (field[1].Value != null) {
          for (let each of field[1].Value.ValuesListIds) {
            data[camelCase(fieldIDs[field[0]])] = await getValuesListValue(fieldDefinitions, field[0], each, authId);
          }
        }
      } else if (type == 8) {
        // console.log('Found! Type 8: ' + field[0]);
        // console.log(field[1].Value);
        data[camelCase(fieldIDs[field[0]])] = [];
        if (field[1].Value != null) {
          for (let each of field[1].Value.UserList) {
            data[camelCase(fieldIDs[field[0]])].push(await getUserListValue(each.Id, authId));          
          }
        }
      }
    }
    // console.log(field[0]);
  }

  // console.log(data);
  // console.log(item);
  // console.log(recordId);
  prettyItem = {
    getItemSuccess: true,
    recordId: recordId,
    createDate: createDate,
    updateDate: updateDate,
    data: data,
    loginSuccess: true
  };
  
  return prettyItem;
}

const getEnvironmentURL = () => {
  if (process.env.GRACIEENVIRONMENT.toUpperCase() == 'PRODUCTION') {
    return `https://gracie/api/core/`
  } else {
    return `https://gracie${process.env.GRACIEENVIRONMENT}/api/core/`;
  }
}

const getAxiosGracieOptions = (url, method, authId=null) => {
  let options = {
    method: `${method.toUpperCase()}`,
    url: `${url}`,
    headers: {
      'Accept': 'application/json, text/html, application/xhtml+xml, application/xml',
      'Content-Type': 'application/json',
      'Authorization': `Archer session-id="${authId}"`
    },
    httpsAgent: new https.Agent({
      rejectUnauthorized: false
    })
  };

  if (url.includes('login')) {
    let environment = process.env.GRACIEENVIRONMENT;
    let username = process.env.GRACIEUSERNAME;
    let password = process.env.GRACIEPASSWORD;
    options.data = {
      'InstanceName': `GRACIE${environment.toUpperCase()}`,
      'UserDomain': '',
      'Username': `${username}`,
      'Password': `${password}`
    };
  }

  if (url.includes('logout')) {
    options.data = {
      'Value': `${authId}`
    }
  }

  return options;
}