const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const ntlm = require('express-ntlm');
const fs = require('fs');
const rfs = require('rotating-file-stream');
const colors = require('colors');
const path = require('path');

require('dotenv').config({ path: 'config.env' });
require('babel-core/register');
require('babel-polyfill');

console.log('Checking for token...');
if (process.env.TFSTOKEN === "" || typeof process.env.TFSTOKEN == 'undefined') {
  console.log('Error!');
  console.log('TFSTOKEN not found in the config file'.red);
  console.log('Exiting...');
  process.exit();
} else { console.log('Found!'.cyan); }

const routes = require('./routes/index');

const app = express();

if (process.env.ENVIRONMENT !== 'dev') {
  app.use(ntlm({
    domain: 'mi.corp.rockfin.com',
    domaincontroller: 'ldap://ldapquery.mi.corp.rockfin.com'
  }));
}

// logging
morgan.token('id', (req, res) => {
  if (process.env.ENVIRONMENT !== 'dev') {
    return req.ntlm.UserName;
  } else {
    return 'JSmith';
  }
});
let tokens = '[:date[clf]] - :id - :status :method :url :response-time ms';
let logDirectory = path.join(__dirname, 'logs');
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
let logStream = rfs('access.log', {
  interval: '1d',
  path: logDirectory
});
app.use(morgan(tokens));
app.use(morgan(tokens, {stream: logStream}));

// parse forms
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(expressValidator());

// serve files from react app after build
app.use(express.static(path.join(__dirname, '..', 'client/build')));

app.use('/', routes);

app.set('port', process.env.PORT || 5000);
const server = app.listen(app.get('port'), () => {
  console.log(`Server running on port ${server.address().port}`);
});

const checkTFSToken = () => {
  
}